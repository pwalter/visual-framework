---
title: Changelog
label: CHANGELOG
order: 200
isIndex: true
---

What we've been working on. You can [view and download all releases](https://git.embl.de/grp-stratcom/visual-framework/tags) on the EMBL GitLab server.

## v2.0-alpha.1

`26 Oct 2018` This release brings more patterns and refinements to patterns and tooling. Also big news is that versioning will now be based on alphas, reflecting the status of development and that this [is version 2 of the Visual Framework](https://blogs.embl.org/communications/2018/09/12/faster-scientific-websites-through-reusability/).

### New and updated patterns
- [Add tabs pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/0cc396200a0528bfb90139b66e43e187e58b44f8)
- [Add fonts (plex mono and sans) as patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/d0a8b6b10fc07f08f5a960268a318b251cfd6327)
- [updated logo pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/3ac72ee65cd20ffcf76fa943645987783915c711)
- [updates vf-tags element](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/d3037a0b3197e2c9733aa71d509b02bb019bbaa4)


### Pattern library edits
- [Copy pattern assets to /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/61074e7a47607dc131df9699b6275e418c3d6e83)
    - This will also have nice knock on affects of patterns being able to bring their own images, fonts, svg, etc.
- [Merge branch 'dev/colorFunction' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/ab500e3f21496a7e46a840cd07411f6731871961)
  - lots of updates, getting ready for NPM mostly
- [fixes linting issue by adding // sass-lint:disable force-element-nesting](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/1bc36c1c7b773ea1266b309a14d07a8b33414079)
- [updates package.json to include urls and filenames](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/7720fd33d16ec16f133071ee9c3c2624eccdd5b8)
- [some per pattern variables](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/6a38da17718178af1e7e678e68aa43d118126972)
- [splits out utility mixins and creates a single import file](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e39f6a348145758f431f9ce5fbad5d1cb8a386a9)
- [updates background color to be the hex version of the ruby red](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e5df9e43f610adcffbf13142575d5ecc12e7ba68)
- [adds squre tag, and adds per pattern variables](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/b250447cd154d6fc49b9e6e079be0b8dc3518d8e)
- [tidies up component view in framework](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/cb8a666418579c0c14336122fe01fd731292dc36)
- [adds index.scss and package.json for utility classes](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/9d6a001ec0bd04be9b8e9fa401d1820f87aa0d78)
- [updates grid column gap so they're all the same](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/873c53da2e664c8ffec498b36be74295469207dc)
- [updates UI of component library](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/b174ea190f5a5b212df6961fdfbb687ddaf35fd1)
- [updates global header to use vf-logo](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/22b7747784f9cd3f7e2b5beff49708e9fd13b664)
- [updates text link colour for UI tabs](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/856a5a3f138512b5f3b683ff45518607ee83d163)
- [updates masthead to use grid and have a black background for heading text](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4869a443f5bf7aeed9afd7738f4beef72573ac37)

### Cleanup
- [ignore /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/49d1121c761bfcede58ef6374c0607e325818121)
- [Move assets into patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/74a965f325a44725688bea514b3d81927bbd80e3)
- [Use dynamic path for patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/042f0e2f60a55252b64d6f618b50f209fcde067d)
- [Generate assets on static site build](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e7e859415acc459ec15829b0b2772945639eea08)
- [Fix an overlow-y scrolling](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e3681a87820c36f685d3c6b38063cd757865a516)
    - Grid inherits `min-width: auto`, strangely this wasn't an issue in Chrome, but noticed in Safari. Was breaking the layout for long code examples.
- [starts adding per pattern variables within the .scss file](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/41bcf45e0be3d82752b5ee03c8ebd13e6d9d5da0)
- [documentation/versioning](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/a6c63d4464663df21eb4ff2824c59b2db2097622)
    - Add some notes about how we plan to version.

## v0.2.2

`19 Oct 2018` This week brings a few new patterns, fixes for the public pattern library, and streamlines pattern asset handling.

### New and updated patterns
- [Patterns/tabs](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/0cc396200a0528bfb90139b66e43e187e58b44f8)
- [Patterns/status](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/fcb84f1bb9c0530f7ee4966c278c8da8bda1bdca)
- [Add Plex Sans and Mono as patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e829c17a5b0e29ac6c172525c136a381ae4466dd)

### Pattern library edits
- [Fix static URL generation](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4ff36912b03734898440a3b610ea86d872f78190)
- [grab compiled css, js other assets](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e853121883c8866a74b0d845fc8ea12b1282a95a)
- [Copy pattern assets to /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/93c2c9500e34e202c9ef593db6b49ee33829cb49)
- [Copy pattern assets to /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/61074e7a47607dc131df9699b6275e418c3d6e83)
- [Use dynamic path for patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/042f0e2f60a55252b64d6f618b50f209fcde067d)
    - Let nunjucks determine the path (so things work for static build and dev build)
- [Generate assets on static site build](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e7e859415acc459ec15829b0b2772945639eea08)
- [Use path syntax compatible with static builds](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/88ea59dc8fddc7dd02b82d90ca263c30ac243bc0)
- [Move assets into patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/74a965f325a44725688bea514b3d81927bbd80e3)
- [Move js into component assets](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/1e67c859554e813e89478637208060cef3d58d26)

### Cleanup
- [Ignore /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/49d1121c761bfcede58ef6374c0607e325818121)
- [Remove public scripts.js](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/264dfea55bcc82c6b8fb30c1a3faa827adad2ffa)


## v0.2.2

`19 Oct 2018` This week brings a few new patterns, fixes for the public pattern library, and streamlines pattern asset handling.

### New and updated patterns
- [Patterns/tabs](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/0cc396200a0528bfb90139b66e43e187e58b44f8)
- [Patterns/status](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/fcb84f1bb9c0530f7ee4966c278c8da8bda1bdca)
- [Add Plex Sans and Mono as patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e829c17a5b0e29ac6c172525c136a381ae4466dd)

### Pattern library edits
- [Fix static URL generation](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4ff36912b03734898440a3b610ea86d872f78190)
- [grab compiled css, js other assets](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e853121883c8866a74b0d845fc8ea12b1282a95a)
- [Copy pattern assets to /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/93c2c9500e34e202c9ef593db6b49ee33829cb49)
- [Copy pattern assets to /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/61074e7a47607dc131df9699b6275e418c3d6e83)
- [Use dynamic path for patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/042f0e2f60a55252b64d6f618b50f209fcde067d)
    - Let nunjucks determine the path (so things work for static build and dev build)
- [Generate assets on static site build](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e7e859415acc459ec15829b0b2772945639eea08)
- [Use path syntax compatible with static builds](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/88ea59dc8fddc7dd02b82d90ca263c30ac243bc0)
- [Move assets into patterns](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/74a965f325a44725688bea514b3d81927bbd80e3)
- [Move js into component assets](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/1e67c859554e813e89478637208060cef3d58d26)

### Cleanup
- [Ignore /public](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/49d1121c761bfcede58ef6374c0607e325818121)
- [Remove public scripts.js](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/264dfea55bcc82c6b8fb30c1a3faa827adad2ffa)


## v0.2.1

`12 Oct 2018` - A massive update over the last several weeks that adds many patterns and refines the pattern library and approaches.

- [link to live site in the README](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/b61eadf3b2306762795d3f5fd47e9f5c47bc9af4)
- [Merge branch 'updates/readme' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/fe3406a0d554e920ae479374b2d640d956b20123)
- [vf-intro pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/1a4074da258edf7153808f608009f3fc20a4e008)
- [Use a class to specify body rules](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/c3ecf3b084a94595a7df6f080e1db7dfa46e7fb7)
- [Wrap patterns in the grid](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/711b1f15da8b67709a309f774190bfa0a7519ee8)
- [first pass at vf-content and new font sizing for monospace](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/a8cd59a99f0e29cae180e697074b720fc9c17575)
- [Merge branch 'features/vf-body' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/142c942bf41fa68a58db5d265c1553e43f0efb67)
- [Features/dogfood theme](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/7aaa732078a80fdeed0dc53622b5663f7a31057e)
- [Merge branch 'updates/typography' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/f76654f340455a64cc964f80463b9ba4a87469de)
- [contact component](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4604b0e64fea58179c3159d89259fd4541e163f7)
- [Merge branch 'features/contact' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/60633ea0f2955291dfbc0b3483f8f7fef2e610c5)
- [updates some things](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/59618460b94b4d6633fa01ffdef6757ce37b5c20)
- [Merge branch 'updates/semanticsandthings' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/bae71ee9b2f1433d21fe53cea2b2af371a06958f)
- [Features/code highlighting](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/2018a75ba0e255a97c57fd401688a964919f97ec)
- [Note what goes where](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/f5935b47b8e6734be8f646d4b3aed899bafe275e)
    - #changelog #documentation #issueQueues
- [Merge branch 'documentation/support-queues' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4d7a0b09d2bbe85b473094a67d4b55990b662e73)
    - Note what goes where
- [Update to fractal npm 1.2.0-beta.1](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/6032b4478edcb6ff201f371ef63c6eb76a14707d)
    - Brings in the nunjucks update
- [Features/sass lint](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e0fba4749b2a77de1d23b98038489f0acc6c8c0c)
    - Features/sass lint
- [Freatures/changelog](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/9a210e526e8e42ab0db303107b304dea7b6af848)
- [updates to HTML, CSS, JS, guidelines](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/6234430da8c8827ee9bc5e0402398f6a5833e692)
- [Merge branch 'dev/guidelines' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/7b8716cf2b1dd080ee20c3aa8e8f494292b4ebb6)
    - adds updates to HTML, CSS, JS, guidelines
- [button-small pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/7a324fecec9d153e153267bc5d9216b555692014)
- [Merge branch 'patterns/button-small' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/06c03e07a7fd129ea267896cbe97e04d4f48d033)
    - button-small pattern
- [updates classnames to take on new  classes](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/505bab8776548e8a3b99fc60747fd92a91d3219f)
- [Merge branch 'updates/classnames' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/b161abe38a8139db15c92b0f780daf04d4098627)
    - updates classnames to take on new  classes
- [summary component](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/a1a58372dcd14ae7e536c1adfd651f0cf9f73813)
- [Patterns/news item](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4ddd96e4dadd62ea97071721fe245bdf439fe82f)
    - Patterns/news item
- [a new 'summary container' pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/271cea7366cbb11de74913fe708b7790dcb2c9fb)
    - adds a new 'summary container' pattern
- [video block pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/a8a64eed39a89c7bb67c554775c22fe3102e47c7)
- [video teaser pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/660ab0972b19c45ef51a30dfcb8232c775402a42)
- [video container](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/17288e376a52f50be1d23b6d4dc455d1108287c9)
- [Merge branch 'pattern/video-container' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/52f10df9468950d4044269375321b2f3288a4927)
    - video container
- [updates new item pattern to include snippet](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e41456219af5fb0bd9fa19e0327452a71bb75981)
- [Merge branch 'pattern/news-sources' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/dee2a80f3488e0d70c6ce7cc277ba55059903a39)
    - updates new item pattern to include snippet
- [Pattern/news container](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/11b9ca2cdeb66ca3efb5c646bf27445537e9bb19)
    - Pattern/news container
- [a  tight links list and links list using secondary colors](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/8c773a9958f0be6def97553cafdaf9c6a227491f)
- [Merge branch 'pattern/category-list' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/fb34b18f8287069d16739b43e8e62496f8559284)
    - adds a  tight links list and links list using secondary colors
- [job description snippet pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/1d53b834c585198ae1c9fef9361d1566462d8bb2)
    - adds job description snippet pattern
- [patterns/vf-utility-classes](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/1f621af7f252c7a5f142d4e95dbabe755e028e5e)
- [patterns/vf-content](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e088d9875a83b5b8dc72dc94b2b5000ea4a01711)
- [splits out buttons to individual parts](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/261f9ffed3711a357b5fedccff92eabb6eac6987)
- [splits out lists](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/058064e2ef405eb0cb1d790664b00a5429523bb2)
- [Merge branch 'patterns/buttons' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/cd22c0760e8f16270efd8075574fc60581ff1f03)
    - splits out buttons to individual parts
- [Use update `vf-code-example` pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/c9debefd6e77adbaa432452e18a16b3be7c0b434)
    - Just a hot fix for the pattern library.
- [Form Elements and Controls](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/109c301eeb3b611b97473cbc5481e9acf5198a98)
- [Merge branch 'patterns/forms' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/0f5079d18fa28ca97b098401542b5f6b4b90f599)
    - Form Elements and Controls
- [a box element](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e9630fa0158e3b6f3692c3efe1f31beb98afabb8)
- [some updates to box](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/9f793e825ce0f766600eda7358eec15fe7c8142a)
- [updtes gulpfile and moves vendor js about and fixes compile issue](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/74d4d0a56d55b6c7fe8943e71d585fc5e8b0c807)
- [Pattern naming guidance](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/227da3e7d681a1895d003b8c356403858f5efc1d)
    - Pattern naming guidance
- [Patterns/box](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/f01148c7c35f0469adacb8b7fc9954b311c2a916)
- [Merge branch 'patterns/box' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/c459948ce2370c43420f3ba9a61f5912b99d7093)
    - Patterns/box
- [Merge branch 'fixes/scriptsCompiling' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/81435e28686e10a9d1a4014168efbffba15b6d29)
    - Fixes/scripts compiling
- [updates component generator script](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/551075084e92616818ddcf2fbdbf0ab039e0742b)
    - updates component generator script
- [features/pattern-layout](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/ac917c3d45e3c7602468247a1b79ad57d95d2e1e)
    - Further uses patterns to do the layout. Needs more work, but gets us closer.
- [vf-content specificity](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/35ca51f4dd03ec141735b460ba28cbd8258572bc)
    - Another workaround for some vf-content issues. Styles applied to attributes can win over classes regardless of order. This causes problems sometimes on things like the the reversed xl header :(
- [images](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/3d53eee08e3192ea3f456dfd6d16730389976039)
- [masthead components as separate things](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/fa34d93ad054d1e15b787c9b40a29fd92c0570ee)
- [renames masthead as block from container](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/6e481cd5c4547836947043093a8e85b71935261e)
- [new header container](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/69297b32ee33a5adb5e34d13b91e71462cac67b6)
- [preview with no grid](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/2203945fc2b30ded0173d1982a56a1bc05e69472)
- [relevant things to Sass file](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e2fd81549be596ee97d100b5d3505dacc7e9d139)
- [Merge branch 'fixes/vf-content-specificity' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/fdf78feaaf614b4103d3eb5f84928ef8a5bfc85d)
    - vf-content specificity
- [Make grids into a pattern](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4a283171059f8b684fb49a48d04c6a421424e9fb)
- [Merge branch 'patterns/site-navigation' into 'develop'](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/dabcdee79eed0a4d61e915bc009cfec08145432d)
    - Visual Framework Masthead and Navigation
- [quick tidy up of some bits](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/bc5197c0e948eed4f943aca898d694b31eae8a7b)
mac-stratcom12:visual-framework-tooling-prototype khawkins$


## v0.2.0

### fixes double mandelbrot

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/b0746d830476801f790e9cdbba73e105e1fbfac0)

---
### Merge branch 'fixes/fractalbuild' into 'develop'
Fixes/fractalbuild

See merge request grp-stratcom/visual-framework-tooling-prototype!42
[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/329fd24f6ab74527fda157b05f63a74231057f3a)

---
### adds compiled CSS for current Blocks

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/4355636a45a08f07c448efe4943935f73d43c41c)

---
### Merge branch 'updates/patternCSS' into 'develop'
adds compiled CSS for current Blocks

See merge request grp-stratcom/visual-framework-tooling-prototype!44
[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/224d23493eced2d8513ca0ad62ab3b926f66b750)

---
### Add vf-text back

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e51f73a651ce75383439608345864b9b9a672d7d)

---
### Feature/gulp build

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/71a221a5f5e1ab107dba50697826140ac3b43750)

---
### Merge branch 'feature/gulp-build' into 'develop'
Feature/gulp build

See merge request grp-stratcom/visual-framework-tooling-prototype!45
[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/31595f66c281ada1e60dad60b6927efa18783b9c)

---
### Fix truncation of utility classes
Classes were being generated one character short, a la `.vf-u-padding__bottom-one`

#changelog

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/25d44e2069eb035a6eb9a6572644f5cd273cf236)

---
### Merge branch 'fixes/utility-truncation' into 'develop'
Fix truncation of utility classes

See merge request grp-stratcom/visual-framework!46
[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/02cb4c157691ceb0115738d2d22a68379a4abff9)

---
### adding spans to grid

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/93949cfb46c5e50865849c24ccb4ffe7743bdbb6)

---
### Fix alignment of vf-grid
For #30

#changelog

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/16c0fe96441a67664deefccba1e98ce363ad133b)

---
### updates vf-grid naming and adds spans

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/635d4c978b56f8d6bb6aa86fa1f4561096af8398)

---
### updates naming convention for sizing of components

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/71c587e64d19e6e314236579bacc3080d841e13f)

---
### adds favicon and associated code and assets

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/252546b6fabcd3751c0eaf985528b652c0c312e8)

---
### adds activity list and updates links list

[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/0152409e05c175579b68f1abb6470321f089dddf)

---
### Merge branch 'features/lists' into 'develop'
adds activity list and updates links list

See merge request grp-stratcom/visual-framework!52
[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/e0dad07b6df4deec3524179daf9d1a731a1136b9)

---
### Merge branch 'develop' into 'master'
weekly update

See merge request grp-stratcom/visual-framework!53
[view commit](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/56bd01301150f83c07aa0556f78ecb0675664bee)

## How to make these release notes?


- Background: https://git.embl.de/grp-stratcom/visual-framework/issues/26
- Generate with:
  ```
  git log 0.2.2..HEAD --pretty=format:'
  - [%s](https://git.embl.de/grp-stratcom/visual-framework-tooling-prototype/commit/%H)
      - %b
  ' --reverse```
